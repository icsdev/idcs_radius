# Setting up the evironment


```
# Please note this server will only run on linux operating systems
# install required packages
yum install -y git gcc openssl-devel bzip2-devel wget

# download python
cd /usr/src
wget https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tgz
tar xzf Python-2.7.15.tgz

# install python
cd Python-2.7.15
./configure --enable-optimizations
make altinstall

# make sure it installed correctly
/usr/local/bin/python2.7 -V

# install pip
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
python2.7 get-pip.py

# make directory for app
cd /opt
mkdir radius-server
cd /opt/radius-server

# either copy code here or pull from git
# cd into code directory

# create virtual environment
# install virtualenv 
pip install virtualenv
virtualenv venv
# activate virtual environment, you can run this same command to deactivate the virtual environment as well
source venv/bin/activate

# install dependencies
pip install pyrad requests
```



# Configuring the server

All configurations for the server are contained in the config.ini file. To configure the script you will need to provide IDCS API information, and RADIUS client host information. Comments in the configuration file should give details on each configuration value.


# Running the server

To run the server you just need to perform the following command while in your python virtual environment
```
python RadiusServer.py
```

Note that for a user to test MFA with this RADIUS server they will already need to be enrolled in the Security Questions factor. As of right now that is the only factor this server supports.

# Tesing the server with a Test Client

For a download link and instructions on using the test client see this link: https://support.secureauth.com/hc/en-us/articles/115000594347-How-To-Test-RADIUS-Using-NTRadPing

Please note that when entering the username and password you should not be sending a State variable with the test client, but when inputting the MFA response the State variable will need to be provided. The value of the state variable will be passed to you after a valid username and password request.

By default the server runs on port 1812 for authentication.