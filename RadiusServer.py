from pyrad import dictionary, packet, server
import requests
import base64
import uuid
import ConfigParser
import logging
import sys

# example server here: https://github.com/wichert/pyrad/blob/master/example/server.py

# read configuration file
config = ConfigParser.ConfigParser()
config.read('config.ini')
log_file = config.get('Logging', 'file')

# set up logging
if log_file == '' or log_file is None:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', stream=sys.stdout)
else:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename=log_file)


class FakeServer(server.Server):

    api_url = ""
    client_id = ""
    client_secret = ""
    access_token = ""
    app_name = ""
    single_line = False
    sl_delimiter = ''
    username_prefix = ''
    states = dict()


    def set_idcs_api_values(self, api_url, client_id, client_secret, app_name):
        self.api_url = api_url
        self.client_id = client_id
        self.client_secret = client_secret
        self.app_name = app_name


    def set_idcs_auth_type(self, single_line, sl_delimiter, username_prefix):
        self.single_line = single_line
        self.sl_delimiter = sl_delimiter
        self.username_prefix = username_prefix


    def obtain_idcs_access_token(self):
        logging.debug("Obtaining IDCS access token")
        call_url = self.api_url + "/oauth2/v1/token"
        authorization_header_value = base64.b64encode(self.client_id + ":" + self.client_secret)
        headers = {
            "cache-control": "no-cache",
            "Authorization": "Basic " + authorization_header_value,
            "Content-Type": "application/x-www-form-urlencoded"
        }
        payload = "grant_type=client_credentials&scope=urn%3Aopc%3Aidm%3A__myscopes__"
        response = requests.post(call_url, data=payload, headers=headers)
        self.access_token = response.json()['access_token']


    def get_request_state(self):
        logging.debug("Obtaining the IDCS request state")
        call_url = self.api_url + "/sso/v1/sdk/authenticate?appName=" + self.app_name
        headers = {
            "Authorization": "Bearer " + self.access_token,
            "Content-Type": "application/json"
        }
        response = requests.get(call_url, headers=headers).json()
        return response['requestState']


    def authenticate_un_pw(self, username, password, request_state):
        logging.debug("Authenticating via username and password")
        call_url = self.api_url + "/sso/v1/sdk/authenticate"
        headers = {
            "Authorization": "Bearer " + self.access_token,
            "Content-Type": "application/json"
        }
        payload = {
            "op": "credSubmit",
            "authFactor": "USERNAME_PASSWORD",
            "credentials": {
                "username": username,
                "password": password
            },
            "requestState": request_state
        }
        response = requests.post(call_url, headers=headers, json=payload)
        logging.debug(str(response.json()))
        return response.json()


    def ans_sec_ques(self, answer, question_id, request_state):
        logging.debug("Validating security question and answer")
        call_url = self.api_url + "/sso/v1/sdk/authenticate"
        headers = {
            "Authorization": "Bearer " + self.access_token,
            "Content-Type": "application/json"
        }
        payload = {
            "op": "credSubmit",
            "authFactor": "SECURITY_QUESTIONS",
            "credentials": {
                "questions":[{
                    "questionId": question_id,
                    "answer": answer
                }]
            },
            "requestState": request_state
        }
        response = requests.post(call_url, headers=headers, json=payload)
        logging.debug(str(response.json()))
        return response.json()


    def ans_totp(self, otp_code, request_state, type='TOTP'):
        logging.debug("Validating OTP code of type %s" % type)
        call_url = self.api_url + "/sso/v1/sdk/authenticate"
        headers = {
            "Authorization": "Bearer " + self.access_token,
            "Content-Type": "application/json"
        }
        payload = {
            "op": "credSubmit",
            "authFactor": type,
            "credentials": {
                "otpCode": otp_code
            },
            "requestState": request_state
        }
        response = requests.post(call_url, headers=headers, json=payload)
        logging.debug(str(response.json()))
        return response.json()


    def authn_pw_totp(self, username, password_otp, request_state, type='TOTP', sl_delimiter = ','):
        logging.debug("Authenticating username and password and validating OTP code provided in one request")
        password = password_otp.split(sl_delimiter)[0]

        logging.debug("Single line delimiter: %s" % sl_delimiter)
        call_url = self.api_url + "/sso/v1/sdk/authenticate"
        headers = {
            "Authorization": "Bearer " + self.access_token,
            "Content-Type": "application/json"
        }
        payload = {
            "op": "credSubmit",
            "authFactor": "USERNAME_PASSWORD",
            "credentials": {
                "username": username,
                "password": password
            },
            "requestState": request_state
        }
        response = requests.post(call_url, headers=headers, json=payload).json()
        logging.debug("IDCS Authentication Response: %s" % response)
        if "authnToken" in str(response.keys()):
            return response
        if response['status'] == "success":
            if response['nextAuthFactors'][0] != "TOTP":
                logging.debug("The user has not been enrolled in a TOTP factor.")
                return None
            logging.debug("Validating OTP Code")
            request_state = response['requestState']
            try:
                otp_code = password_otp.split(sl_delimiter)[1]
            except IndexError:
                logging.info("There was no OTP code provided with the password seperated by a '%s'" % self.sl_delimiter)
                return None
            call_url = self.api_url + "/sso/v1/sdk/authenticate"
            headers = {
                "Authorization": "Bearer " + self.access_token,
                "Content-Type": "application/json"
            }
            payload = {
                "op": "credSubmit",
                "authFactor": type,
                "credentials": {
                    "otpCode": otp_code
                },
                "requestState": request_state
            }
            response = requests.post(call_url, headers=headers, json=payload).json()
            logging.debug("IDCS MFA Validation Response: %s" % response)
            return response
        else:
            # if the response failed we will returned the failed response and _HandleAuthPacket will handle it
            return response


    def _HandleAuthPacket(self, pkt):
        server.Server._HandleAuthPacket(self, pkt)

        logging.info("Received an authentication request")

        # log attributes for debugging
        logging.debug("Attributes: ")
        for attr in pkt.keys():
            logging.debug("%s: %s" % (attr, pkt[attr]))

        # set and display credentials
        logging.debug("Credentials:")
        if self.username_prefix == '' or self.username_prefix is None:
            username = pkt['User-Name'][0].lower()
        else:
            username = pkt['User-Name'][0].split(self.username_prefix)[1].lower()
        pwd = pkt.PwDecrypt(pkt['User-Password'][0])
        logging.debug('Username: %s' % username)
        logging.debug('Password: %s' % pwd)

        # check and see if this request is already part of a request chain
        # if not start a request chain with username and password
        logging.debug("Determining if the request is an authentication or MFA verification")
        if "State" in str(pkt.keys()):
            logging.debug("Request is an MFA verification")
            request_state = self.states[pkt['State'][0]]['req_state']
            factor_type = self.states[pkt['State'][0]]['factor'].split(":")[0]
            if factor_type == 'SECURITY_QUESTION':
                question_id = self.states[pkt['State'][0]]['factor'].split(":")[1]
                response = self.ans_sec_ques(pwd, question_id, request_state)
            elif factor_type == 'TOTP':
                response = self.ans_totp(pwd, request_state)
            elif factor_type == 'SMS':
                response = self.ans_totp(pwd, request_state, type='SMS')
            else:
                reply = self.CreateReplyPacket(pkt, **{
                    "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, 'Could not find valid factor in state')
                })
                reply.code = packet.AccessReject
                logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, 'Could not find valid factor in state'))
            try:
                # if the call isnt successful return a reject
                if response['status'] == "success":
                    reply = self.CreateReplyPacket(pkt, **{
                        "Reply-Message": "Welcome %s!" % (username)
                    })
                    reply.code = packet.AccessAccept
                    logging.debug("RADIUS Response: Welcome %s!" % (username))
                else:
                    cause = response['cause'][0]['message']
                    reply = self.CreateReplyPacket(pkt, **{
                        "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, cause)
                    })
                    reply.code = packet.AccessReject
                    logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, cause))
            except KeyError as e:
                reply = self.CreateReplyPacket(pkt, **{
                    "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, e.message)
                })
                reply.code = packet.AccessReject
                logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, e.message))
        else:
            logging.debug("Request is an Authentication")
            # make authenticate api call with given creds
            request_state = srv.get_request_state()
            if self.single_line:
                response = srv.authn_pw_totp(username, pwd, request_state, sl_delimiter=self.sl_delimiter)
                if response is None:
                    cause = "Either there was no OTP code provided with the password seperated by a '%s', or you are " \
                            "not enrolled in a OTP factor" % self.sl_delimiter
                    reply = self.CreateReplyPacket(pkt, **{
                        "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, cause)
                    })
                    reply.code = packet.AccessReject
                    response = {'status': 'failure'}
            else:
                response = srv.authenticate_un_pw(username, pwd, request_state)
                logging.debug("IDCS Response: %s" % response)
            logging.info("Parsing the response from IDCS")
            try:
                # if the call isnt successful return a reject
                if response['status'] == "success":
                    # check and see if the username and password was all that was required, if not initate MFA
                    if "authnToken" in str(response.keys()):
                        reply = self.CreateReplyPacket(pkt, **{
                            "Reply-Message": "Welcome %s!" % (username)
                        })
                        reply.code = packet.AccessAccept
                    else:
                        if response['nextOp'][0] == "enrollment":
                            cause = "User is required to enroll in MFA before authenticating via RADIUS"
                            reply = self.CreateReplyPacket(pkt, **{
                                "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, cause)
                            })
                            reply.code = packet.AccessReject
                            logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, cause))
                        # do the MFA logic
                        else:
                            # if more than one factor
                            if len(['nextAuthFactors']) > 1:
                                reply = self.CreateReplyPacket(pkt, **{
                                    "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, "Server does not currently support multiple MFA factor options")
                                })
                                reply.code = packet.AccessReject
                                logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, "Server does not currently support multiple MFA factor options"))
                            # currently only supports security questions
                            elif response['nextAuthFactors'][0] == "SECURITY_QUESTIONS":
                                # create key so server can keep track of ongoing auth chains
                                request_state = response['requestState']
                                guid = str(uuid.uuid4())
                                logging.debug("GUID HERE: %s" % guid)
                                self.states[guid] = {
                                    "req_state": request_state,
                                    "factor": "SECURITY_QUESTION:" + response['SECURITY_QUESTIONS']['questions'][0]['questionId']
                                }
                                reply = self.CreateReplyPacket(pkt, **{
                                    "Reply-Message": response['SECURITY_QUESTIONS']['questions'][0]['text'],
                                    "State": guid
                                })
                                reply.code = packet.AccessChallenge
                                logging.debug("RADIUS Response: %s" % response['SECURITY_QUESTIONS']['questions'][0]['text'])
                            elif response['nextAuthFactors'][0] == "TOTP":
                                request_state = response['requestState']
                                guid = str(uuid.uuid4())
                                logging.debug("GUID HERE: %s" % guid)
                                self.states[guid] = {
                                    "req_state": request_state,
                                    "factor": "TOTP"
                                }
                                reply = self.CreateReplyPacket(pkt, **{
                                    "Reply-Message": 'Please enter your OTP code',
                                    "State": guid
                                })
                                reply.code = packet.AccessChallenge
                                logging.debug("RADIUS Response: Please enter your OTP code")
                            elif response['nextAuthFactors'][0] == "SMS":
                                request_state = response['requestState']
                                guid = str(uuid.uuid4())
                                logging.debug("GUID HERE: %s" % guid)
                                self.states[guid] = {
                                    "req_state": request_state,
                                    "factor": "SMS"
                                }
                                reply = self.CreateReplyPacket(pkt, **{
                                    "Reply-Message": 'Please enter your SMS code',
                                    "State": guid
                                })
                                reply.code = packet.AccessChallenge
                                logging.debug("RADIUS Response: Please enter your SMS code")
                            else:
                                reply = self.CreateReplyPacket(pkt, **{
                                    "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, "Unknown MFA factor required")
                                })
                                reply.code = packet.AccessReject
                                logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, "Unknown MFA factor required"))
                else:
                    cause = response['cause'][0]['message']
                    reply = self.CreateReplyPacket(pkt, **{
                        "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, cause)
                    })
                    reply.code = packet.AccessReject
                    logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, cause))
            except KeyError as e:
                reply = self.CreateReplyPacket(pkt, **{
                    "Reply-Message": "Authentication failed for user %s with reason: %s" % (username, e.message)
                })
                reply.code = packet.AccessReject
                logging.debug("RADIUS Response: Authentication failed for user %s with reason: %s" % (username, e.message))

        self.SendReplyPacket(pkt.fd, reply)


if __name__ == '__main__':

    logging.debug("Getting configuration values")
    # get IDCS and RADIUS config values
    idcs_url = config.get('IDCS' , 'url')
    client_id = config.get('IDCS' , 'client_id')
    client_secret = config.get('IDCS', 'client_secret')
    app_name = config.get('IDCS', 'app_name')
    rad_secret = config.get('RADIUS', 'secret')
    rad_port = config.get('RADIUS', 'authport')
    rad_clients = config.get('RADIUS', 'client_ips').split(',')
    username_prefix = config.get('RADIUS', 'username_prefix')
    single_line = config.get('IDCS', 'single_line')
    if single_line.lower() == 'true':
        single_line = True
        sl_delimiter = config.get('IDCS', 'sl_delimiter')
    else:
        single_line = False
        sl_delimiter = ''

    logging.debug("Setting up server variables")
    # create server and read dictionary
    srv = FakeServer(dict=dictionary.Dictionary("dictionary"))

    # set up IDCS API values
    srv.set_idcs_auth_type(single_line, sl_delimiter, username_prefix)
    srv.set_idcs_api_values(idcs_url, client_id, client_secret, app_name)
    srv.obtain_idcs_access_token()

    logging.debug("Adding allowed clients")
    # add clients (address, secret, name, port)
    for client in rad_clients:
        logging.debug("Client: " + client)
        srv.hosts[client] = server.RemoteHost(address=client, secret=b'secret', name=client, authport=rad_port)
    # specify addresses if you wish to only listen on one address
    srv.BindToAddress("")

    logging.info("STARTING SERVER")
    # start server
    srv.Run()